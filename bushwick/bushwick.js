Concerts = new Mongo.Collection("events");

if (Meteor.isClient) {
  // This code only runs on the client
  Template.bw_venues.helpers({
    concerts: function () {
          console.log('Concerts',Concerts.find({}))
          console.log('getEvents',getEvents())
       return getEvents() 
     }
  });

  Template.bw_venue_view.helpers({
    name: "paperbox"
  });

  Template.bwmain.events({
    'click button': function () {
      // increment the counter when button is clicked
    }
  });

  Template.bw_venue_view.helpers({
    font_class:function(){
      var random_number_1 = Math.floor((Math.random() * 21) + 1);
      return "font_" + random_number_1;},
    rotation_class:function(){
      var random_number_2 = Math.floor((Math.random() * 21) + 1);
      return "rotate_" + random_number_2;},
    rotation_class_2:function(){
      var random_number_2 = Math.floor((Math.random() * 6) + 8);
      return "rotate_" + random_number_2;}
    });
}


if (Meteor.isServer) {
  Meteor.startup(function () {
    // code to run on server at startup
  });
}


// $('.bushwick_h2').each(function(){
//   var random_number_1 = Math.floor((Math.random() * 24) + 1);
//   var random_number_2 = Math.floor((Math.random() * 21) + 1);

//   var font_class = "font_" + random_number_1;
//   var rotation_class = "rotate_" + random_number_2;
//   $(this).addClass(font_class).addClass(rotation_class);
//   });
