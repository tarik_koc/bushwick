getEvents = function (){
  return [
    { "artists": "Bastard Noise" 
      , venue: "Silent Barn"
        , dateTime: "12/6"
        , link: "silentbarn.org"
    }
  ,{ "artists": "Weird Womb" 
    , dateTime: "12/6"
      , venue: "Shea Stadium"
  }
  ,{ "artists": "Suburbia" 
    , price: "5 bucks"
      , dateTime: "12/6"
      , link: "facebook.com/playsuburbia"
  }
  ,{ "artists": "Sotheby's on ice" 
    , price: "5 bucks"
      , venue: "Silent Barn"
      , dateTime: "12/7"
      , link: "silentbarn.org"
  }
  ,{ "artists": "Shrapnel" 
    , venue: "acheron"
      , price: "5 bucks"
      , dateTime: "12/7"
      , link: "acheronbk.blogspot.com"
  },
    { name: 'HR (of Bad Brains) // The Scotch Bonnets // The Far East // Dead Tenants',
      venue: 'Palisades',
      dateTime: '12/6',
      location: '906 Broadway, Brooklyn, NY 11206',
      artists: 'HR (of Bad Brains) // The Scotch Bonnets // The Far East // Dead Tenants',
      link: 'https://www.facebook.com/events/1508314659418880/?ref=5' },
      { name: 'Person of Interest // Miguel Enrique Alvariño // Cesar Toribio // Cienfuegos',
        venue: 'Palisades',
        dateTime: '12/6',
        location: '906 Broadway, Brooklyn, NY 11206',
        artists: 'Person of Interest // Miguel Enrique Alvariño // Cesar Toribio // Cienfuegos',
        link: 'https://www.facebook.com/events/296694607195383/?ref=5' },
        { name: 'Thomas Patrick Maguire // Elastic No-No Band // Cannonball Statman // Robot Princess',
          venue: 'Palisades',
          dateTime: '12/6',
          location: '906 Broadway, Brooklyn, NY 11206',
          artists: 'Thomas Patrick Maguirelyn, NY 11206',
          artists: 'Fasano /|\\\\ Adam Evald /|\\\\ Larkin Grimm /|\\\\ Nova Luz',
          link: 'https://www.facebook.com/events/752005774852926/?ref=5' },
          { name: 'Indie Shuffle Showcase: Zula // Paperhaus // Mr. Kid & The Suicide Policemen // Needle Points',
            venue: 'Palisades',
            dateTime: '12/6',
            location: '906 Broadway, Brooklyn, NY 11206',
            artists: 'Indie Shuffle Showcase: Zula // Paperhaus // Mr. Kid & The Suicide Policemen // Needle Points',
            link: 'https://www.facebook.com/events/1519088988335282/?ref=5' },
            { name: 'Pictureplane // Jamaican Queens // Giggly Boys // Jerry Paper ....✞ DJ Mindfreak',
              venue: 'Palisades',
              dateTime: '12/6',
              location: '906 Broadway, Brooklyn, NY 11206',
              artists: 'Pictureplane // Jamaican Queens // Giggly Boys // Jerry Paper ....✞ DJ Mindfreak',
              link: 'https://www.facebook.com/events/1484554321821918/?ref=5' },
              { name: 'Porches // LVL UP // What Moon Things // Cantina',
                venue: 'Palisades',
                dateTime: '12/6',
                location: '906 Broadway, Brooklyn, NY 11206',
                artists: 'Porches // LVL UP // What Moon Things // Cantina',
                link: 'https://www.facebook.com/events/738417772902820/?ref=5' },
                { name: 'Dave Harrington\'s EL TOPO // Friend Roulette // Banned Books // Tiny Hazard',
                  venue: 'Palisades',
                  dateTime: '12/6',
                  location: '906 Broadway, Brooklyn, NY 11206',
                  artists: 'Dave Harrington\'s EL TOPO // Friend Roulette // Banned Books // Tiny Hazard',
                  link: 'https://www.facebook.com/events/1506845769567252/?ref=5' },
                  { name: '☼ THE MEANING OF LIFE ☼ Chiffon // Schwarz // Bebe Panthere // D. Gookin // JX Cannon // Dunes // Special Guest',
                    venue: 'Palisades',
                    dateTime: '12/6',
                    location: '906 Broadway, Brooklyn, NY 11206',
                    artists: '☼ THE MEANING OF LIFE ☼ Chiffon // Schwarz // Bebe Panthere // D. Gookin // JX Cannon // Dunes // Special Guest',
                    link: 'https://www.facebook.com/events/729442610484090/?ref=5' },
                    { name: 'Samuel Cooper // Tiny Beast // Dan Costello // Rob Jennings',
                      venue: 'Palisades',
                      dateTime: '12/6',
                      location: '906 Broadway, Brooklyn, NY 11206',
                      artists: 'Samuel Cooper // Tiny Beast // Dan Costello // Rob Jennings',
                      link: 'https://www.facebook.com/events/1518356638410941/?ref=5' },
                      { name: 'Rat Bastard // Isa Christ // The Powers // Snakehole // Silvia Kastel // Utereye // Mr. Transylvania',
                        venue: 'Palisades',
                        dateTime: '12/6',
                        location: '906 Broadway, Brooklyn, NY 11206',
                        artists: 'Rat Bastard // Isa Christ // The Powers // Snakehole // Silvia Kastel // Utereye // Mr. Transylvania',
                        link: 'https://www.facebook.com/events/295180507340521/?ref=5' },
                        { name: 'Tape Deck Mountain // Writer // Lala Lala // Tele/Visions // Liz Hogg',
                          venue: 'Palisades',
                          dateTime: '12/6',
                          location: '906 Broadway, Brooklyn, NY 11206',
                          artists: 'Tape Deck Mountain // Writer // Lala Lala // Tele/Visions // Liz Hogg',
                          link: 'https://www.facebook.com/events/372445876255285/?ref=5' },
                          { name: 'Gracie // Temple // Nicholas Nicholas // Memorial Gore',
                            venue: 'Palisades',
                            dateTime: '12/6',
                            location: '906 Broadway, Brooklyn, NY 11206',
                            artists: 'Gracie // Temple // Nicholas Nicholas // Memorial Gore',
                            link: 'https://www.facebook.com/events/1564821347083870/?ref=5' },
                            { name: 'Bears // Daddy Issues // Teen Body // Secret Crush',
                              venue: 'Palisades',
                              dateTime: '12/6',
                              location: '906 Broadway, Brooklyn, NY 11206',
                              artists: 'Bears // Daddy Issues // Teen Body // Secret Crush',
                              link: 'https://www.facebook.com/events/1559342214300971/?ref=5' },
                              { name: 'Famous Swords\' Castle of Spices :: Gold Dime▼The Flag▼Parlck ▼ James Thomas Marsh',
                                venue: 'Palisades',
                                dateTime: '12/6',
                                location: '906 Broadway, Brooklyn, NY 11206',
                                artists: 'Famous Swords\' Castle of Spices :: Gold Dime▼The Flag▼Parlor Walls▼CourtshOOR SPORT, SLAM SKILLET, SNACS',
                                link: 'https://www.facebook.com/events/606309489480840/?ref=5' },
                                { name: 'Field Guides // Ben Seretan Group // Prima // Lauds',
                                  venue: 'Palisadesllyfish',
                                  link: 'https://www.facebook.com/events/291366054387293/?ref=5' },
                                  { name: 'Adam Caine // Lutkie // Muyassar Kurdi // Ashcan Orchestra',
                                    venue: 'Palisades',
                                    dateTime: '12/6',
                                    location: '906 Br'}]


}
